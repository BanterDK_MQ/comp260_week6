﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Scorekeeper : MonoBehaviour
{
    public int pointsPerGoal = 1;
    //Array of 2, each players score
    public int[] score = new int[2];
    public Text[] scoreText;

    public Text winner;
    public GameObject winner_GO;


    static private Scorekeeper instance;
    static public Scorekeeper Instance      {get{return instance;}}

    void Start()
    {
        for (int i = 0; i <score.Length; i++)
        {
            //set all scores to 0, for whatever Score(n) is
            score[i] = 0;
            scoreText[i].text = "0";
        }


        ///<summary>
        ///Duplicate check
        /// </summary>
        if (instance == null)
        {
            //save instance
            instance = this;
        }
        else
        {
            Debug.LogError("More than once Scorekeeper exists in the scene");
        }
    }

    public void ScoreGoal(int player)
    {
        //Player Score + x
        score[player] += pointsPerGoal;
        scoreText[player].text = score[player].ToString();

        #region Win Condition
        if (score[player] >= 10)
        {
            //Activate the Winner UI Frame
            winner_GO.SetActive(true);
            //Freeze time
            Time.timeScale = 0;

            if (player == 0)
            {
                winner.text = "Blue wins!";
            }
            else if (player == 1)
            {
                winner.text = "Red wins!";
            }
        }
    }

    public void ReloadScene()
    {
        //un-Freeze time
        Time.timeScale = 1;

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    #endregion  
}
