﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Paddle : MonoBehaviour
{
    public float force = 10f;
    public float speed = 30f;
    public GameObject Paddle;

    public Rigidbody rb;

    public Transform targetPuck;

    private float puckDistance;
    PuckControl puck;


    private Vector3 targetPos;
    private float distToTarget;

    float puck_myGoalDist;
    float puck_enemyGoalDist;


    void Awake()
    {

    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
        puck = GameObject.Find("Puck").GetComponent<PuckControl>();


        targetPos = targetPuck.transform.position;
    }

    void Update()
    {
        puckDistance = Vector3.Distance(targetPuck.position, transform.position);
        puck_myGoalDist = puck.blueGoalDist;
        puck_enemyGoalDist = puck.redGoalDist;
    }


    void FixedUpdate()
    {
        Vector3 moveDir;

        #region Aggressive AI
        ///<summary>
        ///If puck is in the enemy's side of the map
        ///</summary>
        if (puck_enemyGoalDist < puck_myGoalDist)
        {
            moveDir = (targetPuck.position - transform.position);

            rb.AddForce(moveDir * Time.deltaTime * force * speed);
        }
        #endregion


        #region Defensive AI
        ///<summary>
        ///If puck is in our side of the map
        ///</summary>
        if (puck_enemyGoalDist > puck_myGoalDist)
        {
            //Compute next position
            distToTarget = Vector3.Distance(targetPos, transform.position);
            //transform.position = Vector3.MoveTowards(transform.position, targetPos, 0.5f);


            if (puckDistance < 2.5)
            {
                moveDir = (targetPuck.position - transform.position);
                rb.AddForce(moveDir * Time.deltaTime * (force * 1.8f) * (speed * 1.8f));
            }

            //Check current distance
            else if (distToTarget < 1.35)
            {
                targetPos = new Vector3(Random.Range(4.5f, 5.5f), 0, Random.Range(1, -1));
                moveDir = (targetPos - transform.position).normalized;
            }

            else
            {
                moveDir = (targetPos - transform.position).normalized;
            }


            rb.AddForce(moveDir * Time.deltaTime * force * speed);
        }
        #endregion

    }
}
