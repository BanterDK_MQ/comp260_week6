﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour
{
    public Transform startingPos;
    private Rigidbody rb;

    public AudioClip wallCollideClip;
    public AudioClip paddleCollideClip;
    private AudioSource audioPlayer;

    public LayerMask paddleLayer;

    public Transform redGoal;
    public float redGoalDist;

    public Transform blueGoal;
    public float blueGoalDist;


    private void Start()
    {
        audioPlayer = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
        ResetPosition();

    }

    public void Update()
    {
        redGoalDist = Vector3.Distance(redGoal.position, transform.position);
        blueGoalDist = Vector3.Distance(blueGoal.position, transform.position);
    }


    void OnCollisionEnter(Collision col)
    {
        if (paddleLayer.Contains(col.gameObject))
        {
            //We hit the paddle
            audioPlayer.PlayOneShot(paddleCollideClip);
        }

        else
        {
            //We did not hit the paddle
            audioPlayer.PlayOneShot(wallCollideClip);
        }
    }



    public void ResetPosition()
    {
        //Teleport to the starting position
        rb.MovePosition(startingPos.position);

        rb.velocity = Vector3.zero;
        rb.rotation = Quaternion.Euler(0, 0, 0);
    }

    //void OnCollisionStay(Collision col)
    //{
    //    Debug.Log("ColStay: " + col.gameObject.name);
    //}
    //
    //void OnCollisionExit(Collision col)
    //{
    //    Debug.Log("ColExit: " + col.gameObject.name);
    //}


}
