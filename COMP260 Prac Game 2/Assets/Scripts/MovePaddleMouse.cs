﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddleMouse : MonoBehaviour
{
    public float force = 10f;
    public float speed = 30f;
    public GameObject Paddle;

    public Rigidbody rb;


    void Awake()
    {
        //if (Paddle == null)
        //{
        //    Paddle = GameObject.Find("Paddle");
        //}
        //
        //if (Paddle.GetComponent<Rigidbody>() == null)
        //{
        //    rb = Paddle.AddComponent<Rigidbody>();
        //}
    }
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;

    }


    void FixedUpdate()
    {


        Vector3 pos = GetMousePosition();
        Vector3 dir = pos - rb.position;
        Vector3 vel = dir.normalized * speed;

        //speed check -- overshoot
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;

        if (move > distToTarget)
        {
            //scale vel down, appropriately
            vel = vel * distToTarget / move;
        }

        rb.velocity = vel;

        //Vector3 pos = GetMousePosition();
        //Vector3 dir = pos - rb.position;
        //
        //rb.AddForce(dir.normalized * force);

        //Debug.Log("FixedTime = " + Time.fixedTime);
    }

    ///<summary>
    ///Create ray from camera
    ///</summary>
    Vector3 GetMousePosition()

    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Find where ray intercepts XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

    /// <summary>
    /// Raycasting Gizmo, which we will use to draw the mouse ray
    /// </summary>
    void OnDrawGizmos()
    {
        //Draw mouse ray
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }



}


