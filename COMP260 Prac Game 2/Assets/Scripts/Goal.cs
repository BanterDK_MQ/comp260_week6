﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour
{
    //Who gets points for scoring in this goal
    public int player;

    public AudioClip scoreClip;
    private AudioSource audioPlayer;

    void Start()
    {
        audioPlayer = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider col)
    {
        //play score sound
        audioPlayer.PlayOneShot(scoreClip);

        //Tell scorekeeper
        Scorekeeper.Instance.ScoreGoal(player);

        //Get & Reset puck pos
        PuckControl puck = col.gameObject.GetComponent<PuckControl>();
        puck.ResetPosition();
        

    }
}
