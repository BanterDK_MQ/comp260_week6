Transform vs. Rigidbody in movement

Transform: Movement is co-ordinate based, on the X,Y and Z axis (and sometimes, is influenced by rotation)

Rigidbody: Uses physics to calculate movement/position (speed, mass, weight, gravity etc)
-------------------------------------------------------------------

Transform.position:
The position of the GameObject. In the Inspector, this is shown through the X,Y and Z co-ordinates -- Vector3.

Rigidbody.position: 
Allows you to get/set the position of a Rigidbody, using Unity physics. The transform of this will be updated with each physics simulation step, which is faster than updating through Transform.position (since the colliders will have to be calculated relative to the RB).

Rigidbody.velocity: 
The velocity of the rigidbody. Similar, but not the same, as AddForce, as its simply a measurement of movement (and not the position itself)

Rigidbody.AddForce:
Basically pushing the object along by a value of x. This is pretty useful for jumping, as an example.
